eclipse-egit (3.7.0-2) unstable; urgency=medium

  * Team upload.
  * Fixed the plugin id for slf4j (Closes: #813419)
  * Standards-Version updated to 3.9.8
  * Use secure Vcs-* URLs

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 19 Oct 2016 23:15:10 +0200

eclipse-egit (3.7.0-1) unstable; urgency=medium

  * New Upstream release.
  * Refreshed d/patches/use-distribution-jgit.patch.
  * Drop d/patches/build-without-e4.patch
    - e4 bundle import was made optional upstream.
  * Bump Standards-Version to 3.9.6.
  * Override false-positive codeless-jar Lintian warning.
  * Remove Niels Thykier from Uploaders; thanks for your contributions.
  * Add slf4j-api to orbitdeps.

 -- Jakub Adam <jakub.adam@ktknet.cz>  Sun, 26 Apr 2015 21:49:47 +0200

eclipse-egit (3.4.0-1) unstable; urgency=medium

  * Team upload.

  [ Jakub Adam ]
  * New Upstream release.
  * Refreshed d/patches/use-distribution-jgit.patch.
  * Fix build without e4 CSS.

  [ tony mancill ]
  * Update version of libjgit-java build-dep to >= 3.4
  * Use debhelper 9.

 -- tony mancill <tmancill@debian.org>  Sun, 22 Jun 2014 10:47:15 -0700

eclipse-egit (3.3.1-1) unstable; urgency=medium

  * New Upstream release.
  * Refreshed d/patches/use-distribution-jgit.patch.

 -- Jakub Adam <jakub.adam@ktknet.cz>  Wed, 26 Mar 2014 19:50:40 +0100

eclipse-egit (3.3.0-1) unstable; urgency=medium

  [ Jakub Adam ]
  * New Upstream release.
  * Refreshed d/patches/use-distribution-jgit.patch.
  * Removed unused lintian overrides.

  [ tony mancill ]
  * Update libjgit-java dependency version to >= 3.3.0.

 -- Jakub Adam <jakub.adam@ktknet.cz>  Thu, 06 Mar 2014 18:38:20 +0100

eclipse-egit (3.2.0-1) unstable; urgency=medium

  * New Upstream release.
  * Refreshed d/patches/use-distribution-jgit.patch.
  * Bump Standards-Version to 3.9.5.
  * Make Vcs-* fields canonical.

 -- Jakub Adam <jakub.adam@ktknet.cz>  Mon, 23 Dec 2013 19:47:01 +0100

eclipse-egit (3.1.0-1) unstable; urgency=low

  * New Upstream release.
  * Refreshed d/patches/use-distribution-jgit.patch.

 -- Jakub Adam <jakub.adam@ktknet.cz>  Tue, 08 Oct 2013 20:56:55 +0200

eclipse-egit (3.0.3-1) unstable; urgency=low

  * New Upstream release.
  * Refreshed d/patches/use-distribution-jgit.patch.
  * Update libjgit-java dependency >= 3.0.3.

 -- Jakub Adam <jakub.adam@ktknet.cz>  Tue, 17 Sep 2013 20:02:04 +0200

eclipse-egit (3.0.1-1) unstable; urgency=low

  * New Upstream release.
  * Refreshed d/patches/use-distribution-jgit.patch.
  * Add JavaEWAH.jar into orbit dependencies.
  * Use xz for upstream tarball compression.
  * Add org.eclipse.jgit.java7 to orbitdeps.

 -- Jakub Adam <jakub.adam@ktknet.cz>  Wed, 14 Aug 2013 18:58:43 +0200

eclipse-egit (2.3.1-2) unstable; urgency=low

  * Team upload.
  * Upload to unstable for jessie release cycle.

 -- tony mancill <tmancill@debian.org>  Sun, 05 May 2013 19:11:48 -0700

eclipse-egit (2.3.1-1) experimental; urgency=low

  * New Upstream release.
  * Refreshed d/patches/use-distribution-jgit.patch.
  * Update Build-Dep on libjgit-java to 2.3.1.

 -- Jakub Adam <jakub.adam@ktknet.cz>  Fri, 22 Feb 2013 17:26:28 +0100

eclipse-egit (2.2.0-1) experimental; urgency=low

  * New Upstream release.
  * Update Build-Dep on libjgit-java to 2.2.0.
  * Refreshed d/patches/use-distribution-jgit.patch.

 -- Jakub Adam <jakub.adam@ktknet.cz>  Thu, 20 Dec 2012 22:36:33 +0100

eclipse-egit (2.1.0-1) unstable; urgency=low

  * New Upstream release.
  * Refreshed use-distribution-jgit.patch.
  * Updated d/watch.
  * Bump Standards-Version to 3.9.4.

 -- Jakub Adam <jakub.adam@ktknet.cz>  Thu, 27 Sep 2012 18:13:45 +0200

eclipse-egit (2.0.0-1) unstable; urgency=low

  * New Upstream version.
  * Refreshed d/patches/use-distribution-jgit.patch.
  * d/control: replace hardcoded libjgit-java version with
    ${orbit:Depends}.
  * Bump Standards-Version to 3.9.3.
  * Enable EGit Mylyn integration.

 -- Jakub Adam <jakub.adam@ktknet.cz>  Sun, 24 Jun 2012 12:39:57 +0200

eclipse-egit (1.3.0-1) unstable; urgency=low

  * New Upstream version.
  * Add d/watch

 -- Jakub Adam <jakub.adam@ktknet.cz>  Sat, 18 Feb 2012 19:54:41 +0100

eclipse-egit (1.2.0-1) unstable; urgency=low

  * New Upstream version. (Closes: #647009)
  * Transition from eclipse-jgit to libjgit-java.

 -- Jakub Adam <jakub.adam@ktknet.cz>  Fri, 23 Dec 2011 23:36:41 +0100

eclipse-egit (1.1.0-1) unstable; urgency=low

  * Initial release. (Closes: #575783)

 -- Jakub Adam <jakub.adam@ktknet.cz>  Mon, 26 Sep 2011 20:50:30 +0200
